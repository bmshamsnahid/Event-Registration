import {Injectable, OnInit} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Participant} from "../participent/participant";

@Injectable()
export class ParticipantService {
  constructor(private http: Http) {}

  createParticipants(participant: Participant) {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({
      headers: headers
    });

    // https://nean-event-registration.herokuapp.com/participant

    return this.http.post(`https://nean-event-registration.herokuapp.com/participant`, JSON.stringify(participant), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log('Error in dashboard Service: ' + error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }
}

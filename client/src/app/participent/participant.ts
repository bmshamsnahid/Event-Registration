export class Participant {
  _id: string;
  name: string;
  rollNo: string;
  fatherName: string;
  dateOfBirth: Date;
  address: string;
  email: string;
  phoneNumber: string;
  educationalQualification: string;
  isApproved: boolean;
}

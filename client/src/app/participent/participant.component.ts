import {Component, OnInit} from "@angular/core";
import {ToastrService} from "../common/toastr.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Participant} from "./participant";
import {ParticipantService} from "./participant.service";
import {Router} from "@angular/router";


@Component({
  templateUrl: './participant.component.html'
})
export class ParticipantComponent implements OnInit{

  minDate = new Date(2000, 0, 1);
  maxDate = new Date(1990, 0, 1);

  participant: Participant;

  constructor(private toastr: ToastrService,
              private participantService: ParticipantService,
              private router: Router) {}

  ngOnInit() {
    this.participant = new Participant();
  }

  onClickJoin(): void {
    console.log('On Click Join Button Clicked.');
    console.log(this.participant);
    console.log('Validation: ' + this.checkValidation(this.participant));
    if (this.checkValidation(this.participant) == true) {
      this.participantService.createParticipants(this.participant).subscribe((data) => {
        if (data.success) {
          this.toastr.success('Successfully registered to the event.');
          this.router.navigate(['/home']);
        } else {
          this.toastr.warning('Error in registration: ' + data.message);
          this.toastr.info('Please try again.');
        }
      });
    }
  }

  checkValidation(participant: Participant): boolean {

    let isValidated: boolean = true;

    if (!participant.name || !this.participant.rollNo ||
      !this.participant.fatherName || !this.participant.dateOfBirth ||
      !participant.address || !participant.email ||
      !this.participant.phoneNumber || !participant.educationalQualification) {
      this.toastr.warning('Invalid/Incomplete Information.');
      return false;
    }

    if (participant.name.length <= 2) {
      this.toastr.warning('Name length must be at least 5 character');
      return false;
    }

    if (participant.fatherName.length <= 0) {
      this.toastr.warning('Fathers Name can not be empty.');
      return false;
    }

    if (this.validateEmail(this.participant.email) == false) {
      this.toastr.warning('Invalid Email Address');
      return false;
    }

    if (/^\d+$/.test(participant.rollNo) == false) {
      this.toastr.warning('Invalid Roll Number');
      return false;
    }

    if (/^\d+$/.test(participant.phoneNumber) == false) {
      this.toastr.warning('Invalid Phone Number');
      return false;
    }

    if (participant.educationalQualification.length <= 0) {
      this.toastr.warning('Educational qualifications can not be empty.');
      return false;
    }

    if (participant.address.length <= 0) {
      this.toastr.warning('Address can not be empty.');
      return false;
    }

    if (this.validateDate(this.participant.dateOfBirth) == false) {
      this.toastr.warning('Invalid Date of birth.');
      return false;
    }

    return true;
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validateDate (dateOfBirth: Date): boolean {
    let isValidDate: boolean = true;

    const birthYear = dateOfBirth.getFullYear(),
          currentYear = new Date().getFullYear(),
          diff = currentYear - birthYear;

    if (diff <= 17) isValidDate = false;

    return isValidDate;
  }

}

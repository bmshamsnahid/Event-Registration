import {Component, OnInit} from "@angular/core";
import {AuthService} from "../auth/auth.service";
import {AdminService} from "./admin.service";

@Component({
  templateUrl: './admin.component.html',
  styleUrls: [
    './admin.component.css'
  ]
})
export class AdminComponent implements OnInit {

  constructor(private authService: AuthService,
              private adminService: AdminService) {}

  ngOnInit() {}

}

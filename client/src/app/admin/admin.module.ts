import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {AdminComponent} from "./admin.component";
import {AuthService} from "../auth/auth.service";
import {AuthGuardService} from "../auth/auth-guard.service";
import {AdminService} from "./admin.service";
import {NgModule} from "@angular/core";
import {LoginComponent} from "../auth/login.component";
import {LogoutComponent} from "../auth/logout.component";
import {DashboardComponent} from "../dashboard/dashboard.component";
import {DashboardService} from "../dashboard/dashboard.service";
import {ToastrService} from "../common/toastr.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'dashboard', canActivate:[ AuthGuardService ], component: DashboardComponent },
      { path: 'login', component: LoginComponent },
      { path: 'logout', canActivate:[ AuthGuardService ], component: LogoutComponent }
    ]),
  ],
  declarations: [
    AdminComponent,
    LoginComponent,
    DashboardComponent,
    LogoutComponent
  ],
  providers: [
    AuthService,
    AuthGuardService,
    AdminService,
    DashboardService,
    ToastrService
  ]
})
export class AdminModule {}

import {Injectable, OnInit} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Participant} from "../participent/participant";

@Injectable()
export class DashboardService implements OnInit {

  public token: string;

  constructor(private http: Http) {
    const cAdminObj = localStorage.getItem('cAdmin');
    const cAdmin = JSON.parse(cAdminObj);
    this.token = cAdmin.token;
  }

  ngOnInit() { }

  getParticipants() {
    const headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.token}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.get(`https://nean-event-registration.herokuapp.com/participant`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  updateParticipant(participant: Participant) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.token}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.patch(`https://nean-event-registration.herokuapp.com/participant/${participant._id}`, JSON.stringify(participant), options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  deleteParticipant(participant) {
    const headers = new Headers();
    headers.append('Authorization', `${this.token}`);
    const options = new RequestOptions({
      headers: headers
    });

    return this.http.delete(`https://nean-event-registration.herokuapp.com/participant/${participant._id}`, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log('Error in dashboard Service: ' + error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }

}

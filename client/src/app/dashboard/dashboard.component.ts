import {Component, OnInit} from "@angular/core";
import {AuthService} from "../auth/auth.service";
import {ToastrService} from "../common/toastr.service";
import {Admin} from "../admin/admin";
import {DashboardService} from "./dashboard.service";
import {Participant} from "../participent/participant";
import {Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './dashboard.component.css'
  ]
})
export class DashboardComponent implements OnInit {

  admin: any;
  participants: Participant[] = [];
  cParticipant: Participant;

  constructor(private authService: AuthService,
              private toastr: ToastrService,
              private dashboardService: DashboardService,
              private router: Router,
              private location: Location) {}

  ngOnInit() {
    this.cParticipant = new Participant();
    this.admin = localStorage.getItem('cAdmin');
    this.getAllParticipants();
  }

  getAllParticipants() {
    this.dashboardService.getParticipants().subscribe((data) => {
      this.participants = data.participants;
      console.log(this.participants);
    });
  }

  onClickParticipant(participant: Participant) {
    console.log(participant);
    this.cParticipant = participant;
  }

  onClickChangeApproval(participant: Participant) {
    participant.isApproved = !participant.isApproved;
    this.dashboardService.updateParticipant(participant).subscribe((data) => {
      console.log('Retrived Data');
      console.log(data);
    });
  }

  onClickUpdateParticipant() {
    this.dashboardService.updateParticipant(this.cParticipant).subscribe((data) => {
        if (data.success == true) {
          this.toastr.success('Successfully updated the participant Informattion');
        } else {
          this.toastr.error('Error in updating participant information: ' + data.message);
        }
      });
    }

  onClickDeleteParticipant() {
    this.dashboardService.deleteParticipant(this.cParticipant).subscribe((data) => {
      if (data.success == true) {
        this.toastr.success('Successfully deleted the participant Informattion');
        this.participants.slice(this.participants.indexOf(this.cParticipant), 1);
        // this.location.reload();
      } else {
        this.toastr.error('Error in deleting participant information: ' + data.message);
      }

    });
  }

}

import {Component} from "@angular/core";
import {AuthService} from "./auth.service";
import {Admin} from "../admin/admin";
import {ToastrService} from "../common/toastr.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css'
  ]
})
export class LoginComponent {

  admin: Admin;

  constructor(private authService: AuthService,
              private toastr: ToastrService,
              private router: Router) {}

  ngOnInit() {
    this.admin = new Admin();
  }

  onClickLogIn() {
    console.log('Email: ' + this.admin.email);
    console.log('Password: ' + this.admin.password);

    this.authService.login(this.admin).subscribe((data) => {
      console.log(data);
      if (data.success === false) {
        this.toastr.error(data.message);
      } else {
        this.router.navigate(['dashboard'])
      }
    });

  }
}

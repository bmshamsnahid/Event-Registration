import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {AuthService} from "./auth.service";
import {ToastrService} from "../common/toastr.service";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService,
              private router: Router,
              private toastr: ToastrService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return this.checkLoggedIn(state.url);
  }

  checkLoggedIn(url: string) {
    if (this.authService.isLoggedIn()) return true;
    this.toastr.info('Go to login page to access dashboard.')
    this.router.navigate(['/login']);
    return false;
  }

}

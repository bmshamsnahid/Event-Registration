import {Component, OnInit} from "@angular/core";
import {ToastrService} from "../common/toastr.service";
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";

@Component({
  template: ''
})
export class LogoutComponent implements OnInit {
  constructor(private authService: AuthService,
              private router: Router,
              private toastr: ToastrService) {}

  ngOnInit() {
    this.authService.logout();
    this.toastr.success('You have successfully logged out.');
    this.router.navigate(['home']);
  }
}

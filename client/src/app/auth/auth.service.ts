import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Admin} from "../admin/admin";
import {Http, RequestOptions, Response, Headers} from "@angular/http";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthService {
  public cAdmin: Admin;
  loginUrl = 'https://nean-event-registration.herokuapp.com/auth/login';
  constructor(private http: Http) {}

  isLoggedIn(): boolean {
    try {
      const theUser: any = JSON.parse(localStorage.getItem('cAdmin'));
      if (theUser) {
        this.cAdmin = theUser;
        return true;
      }
    } catch (e) {
      return false;
    }

    return false;
  }

  login(cAdmin) {
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});

    return this.http.post(this.loginUrl, JSON.stringify(cAdmin), options)
      .do((response: Response) => {
        console.log('Response on login attempt: ');
        console.log(response);
        if (response.json().success) {
          this.cAdmin = response.json().message;
          let cAdmin: any = {};
          cAdmin.user = response.json().message;
          cAdmin.token = response.json().token;

          localStorage.setItem('cAdmin', JSON.stringify(cAdmin));
        }
        response.json();
      })
      .catch(this.handleError);
  }

  logout(): void {
    this.cAdmin = null;
    localStorage.removeItem('cAdmin');
  }

  private handleError(error: Response) {
    console.log('Error is: ' + error);
    return Observable.throw(error.json().error || 'Server Error');
  }
}

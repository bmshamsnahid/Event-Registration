import { Component } from '@angular/core';
import {ToastrService} from "./common/toastr.service";
import {AuthService} from "./auth/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private toasterService: ToastrService,
              public authService: AuthService) {
    console.log('Log in status: ' + this.authService.isLoggedIn());
    // this.toasterService.info('Wellcome to Event Registration System', 'Wellcome');
  }
}

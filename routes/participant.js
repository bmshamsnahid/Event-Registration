var express = require('express'),
    router = express.Router(),
    participantController = require('../controller/participant'),
    authController = require('../controller/auth');

router.post('/', participantController.createParticipant);
router.get('/:id', participantController.getSingleParticipant);
router.get('/', authController.authenticate, participantController.getAllParticipant);
router.patch('/:id', participantController.updateParticipant);
router.delete('/:id', participantController.deleteParticipant);

module.exports = router;
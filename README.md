# [![MEAN Logo](http://mean.io/wp-content/themes/twentysixteen-child/images/meanlogo.png)](http://mean.io/) MEAN<sup>2</sup> Event Registration System


## Prerequisite Technologies In Your Machine
* [Node.js](https://nodejs.org/en/)
* [Angular4 web starter](https://angular.io/)
* [MongoDB](https://www.mongodb.com)
* [Express](https://expressjs.com/)
* [Git](https://git-scm.com/downloads)
* [npm](https://www.npmjs.com/)

## Installation

To start your application with MEAN, you need to clone the base MEAN repository from Github. This repository contains all the packages, modules and also a sample code base in order to start and make it easy to develop your application. Following the steps below will guide you to install the latest MEAN version.

```
https://github.com/bmshamsnahid/meanEventRegistration.git  
cd meanEventRegistration
npm install
node server.js
```
Open Another Terminal in root directory
```
cd client
npm install
ng serve
```

Now Server is running on port 8080 and Client is running on port 4200.
Go to localhost:4200 and Test the application.
const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    cors = require('cors'),
    path = require('path');

const config = require('./config'),
    authRoutes = require('./routes/auth'),
    participantRoutes = require('./routes/participant'),
    adminRoutes = require('./routes/admin');

var port  = process.env.PORT || config.serverPort;

app.use(express.static(path.join(__dirname, 'public/dist')));

mongoose.Promise = global.Promise;
mongoose.connect(config.database, (err) => {
    if (err) console.log('Error in database connection: ' + err);
    else console.log('Database Connected Successfully');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(morgan('dev'));

app.use(cors());

app.use('/auth', authRoutes);
app.use('/participant', participantRoutes);
app.use('/admin', adminRoutes);

app.listen(port);
console.log('App is running on port: ' + port);

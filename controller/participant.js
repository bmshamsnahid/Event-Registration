const mongoose = require('mongoose'),
    Participant = require('../model/participant'),
    nodemailer = require('nodemailer'),
    api_key = 'key-e9bcddadc61782377921a30b325efb1a',
    domain = 'sandboxf0524bf7cb564ded9d097334d53a02e9.mailgun.org',
    mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
//
// var sendgrid = require("sendgrid")("y2riHJeHRfyblBn2679oSQ");
// var email = new sendgrid.Email();
//
// email.addTo("test@sendgrid.com");
// email.setFrom("you@youremail.com");
// email.setSubject("Event Registration");
// email.setHtml("Thank you for your registration.");
//
// sendgrid.send(email);

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('y2riHJeHRfyblBn2679oSQ');

const createParticipant = (req, res, next) => {

    const name = req.body.name,
        rollNo = req.body.rollNo,
        fatherName = req.body.fatherName,
        dateOfBirth = req.body.dateOfBirth,
        address = req.body.address,
        email = req.body.email,
        phoneNumber = req.body.phoneNumber,
        educationalQualification = req.body.educationalQualification;

    // console.log(req.body);

    if (!name || !rollNo || !fatherName || !dateOfBirth || !address ||
        !email || !phoneNumber || !educationalQualification) {
        res.status(201).json({
            success: false,
            message: 'Invalid or Incomplete Parameter'
        });
    } else {
        checkIfUserExist(rollNo, email, req, res, next, (err, req, res, next) => {
            if (err) {
                res.status(400).json({
                    success: false,
                    message: 'Error in user existence check: ' + err
                });
            } else {
                let participant = new Participant({
                    name: name,
                    rollNo: rollNo,
                    fatherName: fatherName,
                    dateOfBirth: dateOfBirth,
                    address: address,
                    email: email,
                    phoneNumber: phoneNumber,
                    educationalQualification: educationalQualification,
                    isApproved: false
                });

                participant.save((err, newParticipant) => {
                    if (err) {
                        return res.status(400).json({
                            success: false,
                            message: 'Fatal Server error: ' + err
                        });
                    } else {
                        // const msg = {
                        //     to: 'test@example.com',
                        //     from: 'mr.event.register.ninja@gmail.com',
                        //     subject: 'Sending with SendGrid is Fun',
                        //     text: 'and easy to do anywhere, even with Node.js',
                        //     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
                        // };
                        // sgMail.send(msg);
                        return res.status(201).json({
                            success: true,
                            message: 'Successfully created new participant',
                            data: newParticipant
                        });

                        // var data = {
                        //     from: 'mailgun@https://app.mailgun.com/app/domains/nean-event-registration.herokuapp.com',
                        //     to: newParticipant.email,
                        //     subject: 'Event Registation Notifier' ,
                        //     text: 'Thank you for register to the event. You will be notifoed as soon as you being approved.'
                        // };
                        //
                        // mailgun.messages().send(data, function (error, body) {
                        //     if (error) {
                        //         console.log(error);
                        //         return res.status(400).json({
                        //             success: false,
                        //             message: 'Fatal Email Server error: ' + err
                        //         });
                        //     } else {
                        //         return res.status(201).json({
                        //             success: true,
                        //             message: 'Successfully created new participant',
                        //             data: newParticipant
                        //         });
                        //     }
                        // });
                    }
                });
            }
        });
    }
};

const getSingleParticipant = (req, res, next) => {
    const participantId = req.params.id;

    if (!participantId) {
        res.status(201).json({
            success: false,
            message: 'Invalid or Incomplete Parameter'
        });
    } else {
        Participant.findById(participantId, (err, participant) => {
            if (err) {
                res.status(400).json({
                    success: false,
                    message: 'Fatal Server error: ' + err
                });
            } else if(participant) {
                res.status(201).json({
                    success: true,
                    message: 'Successfully got the participant information',
                    participant: participant
                });
            } else {
                res.status(201).json({
                    success: false,
                    message: 'Invalid or Incomplete Infornation.'
                });
            }
        });
    }
};

const getAllParticipant = (req, res, next) => {
    Participant.find((err, participants) => {
        if (err) {
            res.status(400).json({
                success: false,
                message: 'Fatal Server error: ' + err
            });
        } else {
            res.status(201).json({
                success: true,
                message: 'Successfully got al the participants information',
                participants: participants
            });
        }
    });
};

const updateParticipant = (req, res, next) => {
    const name = req.body.name,
        rollNo = req.body.rollNo,
        fatherName = req.body.fatherName,
        dateOfBirth = req.body.dateOfBirth,
        address = req.body.address,
        email = req.body.email,
        phoneNumber = req.body.phoneNumber,
        educationalQualification = req.body.educationalQualification,
        isApproved = req.body.isApproved,
        participantId = req.params.id;

    console.log("In server body: ");
    console.log(req.body);

    if (!name || !rollNo || !fatherName || !dateOfBirth || !address ||
        !email || !phoneNumber || !educationalQualification || !participantId ) {
        res.status(201).json({
            success: false,
            message: 'Invalid or Incomplete Parameter'
        });
    } else {
        checkIfUpdateUserExist(rollNo, email, participantId, req, res, next, (err, req, res, next) => {
            const currentApprovalState = req.body.isApproved;
            let previousApprovalState;

            if (err) {
                res.status(201).json({
                    success: false,
                    message: err
                });
            } else {
                Participant.findById(participantId, (err, participant) => {
                    if (err) {
                        res.status(400).json({
                            success: false,
                            message: 'Fatal Server error: ' + err
                        });
                    } else {
                        previousApprovalState = participant.isApproved;
                        participant.name = req.body.name || participant.name;
                        participant.rollNo = req.body.rollNo || participant.rollNo;
                        participant.fatherName = req.body.fatherName || participant.fatherName;
                        participant.dateOfBirth = req.body.fatherName || participant.dateOfBirth;
                        participant.address = req.body.address || participant.address;
                        participant.email = req.body.email || participant.email;
                        participant.phoneNumber = req.body.phoneNumber || participant.phoneNumber;
                        participant.educationalQualification = req.body.educationalQualification || participant.educationalQualification;
                        participant.isApproved = isApproved;

                        if (currentApprovalState == true && previousApprovalState == false) {
                            const myRandomNumber = (Math.random() * (100000000000 - 1000000000) + 1000000000).toString();
                            participant.trackingNumber = myRandomNumber + participant._id;
                        }

                        participant.save((err, updatedParticipant) => {

                            if (err) {
                                res.status(400).json({
                                    success: false,
                                    message: 'Fatal Server error: ' + err
                                });
                            } else {
                                let data, isEmailRequired = false;

                                if (currentApprovalState == true && previousApprovalState == false) {
                                    data = {
                                        from: 'mailgun@sandboxf0524bf7cb564ded9d097334d53a02e9.mailgun.org',
                                        to: updatedParticipant.email,
                                        subject: 'Event Registration Confirmation' ,
                                        text: 'Thank you for registration. You are now approved by the admin panel. And your tracking number is: ' + updatedParticipant.trackingNumber
                                    };
                                    isEmailRequired = true;
                                } else if (currentApprovalState == false && previousApprovalState == true) {
                                    data = {
                                        from: 'mailgun@sandboxf0524bf7cb564ded9d097334d53a02e9.mailgun.org',
                                        to: updatedParticipant.email,
                                        subject: 'Event Registration Denial' ,
                                        text: 'Sorry to notify that you being removed from the participant approval list.'
                                    };
                                    isEmailRequired = true;
                                }

                                isEmailRequired = false;

                                if (isEmailRequired) {
                                    mailgun.messages().send(data, function (error, body) {
                                        if (error) {
                                            console.log(error);
                                            return res.status(400).json({
                                                success: false,
                                                message: 'Fatal Email Server error: ' + err
                                            });
                                        } else {
                                            return res.status(201).json({
                                                success: true,
                                                message: 'Successfully updated the participant',
                                                data: updatedParticipant
                                            });
                                        }
                                    });
                                } else {
                                    return res.status(201).json({
                                        success: true,
                                        message: 'Successfully updated the participant information',
                                        participant: updatedParticipant
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });
    }
};

const deleteParticipant = (req, res, next) => {
    const participantId = req.params.id;

    if (!participantId) {
        res.status(201).json({
            success: false,
            message: 'Invalid or Incomplete Infornation.'
        });
    }

    Participant.findByIdAndRemove(participantId, (err) => {
        if (err) {
            res.status(400).json({
                success: false,
                message: 'Fatal Server error: ' + err
            });
        } else {
            res.status(201).json({
                success: true,
                message: 'Successfully deleted the participant.'
            });
        }
    });
};

module.exports = {
  createParticipant,
  getSingleParticipant,
  getAllParticipant,
  updateParticipant,
  deleteParticipant
};

const checkIfUserExist = (rollNo, email, req, res, next,  cb) => {
    Participant.find({
        $or: [
            { rollNo: rollNo},
            { email: email }
        ]
    }, (err, results) => {
        if (err) {
            return cb('Fatal Server error: ' + err, req, res, next);
        } else if(results) {
            if (results.length <= 0) {
                return cb(null, req, res, next);
            } else {
                return cb('Roll Number or Email Already Exists.', req, res, next);
            }
        }
    });
};

const checkIfUpdateUserExist = (rollNo, email, userId, req, res, next,  cb) => {
    console.log('update user: ' + userId);
    Participant.find({
        $or: [
            { rollNo: rollNo},
            { email: email }
        ], $nor: [
            { _id: userId }
        ]
    }, (err, results) => {
        if (err) {
            return cb('Fatal Server error: ' + err, req, res, next);
        } else if(results) {
            if (results.length <= 0) {
                return cb(null, req, res, next);
            } else {
                return cb('Roll Number or Email Already Exists.', req, res, next);
            }
        }
    });
};

const mongoose = require('mongoose'),
    Admin = require('../model/admin'),
    jwt = require('jsonwebtoken'),
    config = require('../config');

const authenticate = (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['authorization'];
  if (token) {
      jwt.verify(token, config.secret, (err, decoded) => {
          if (err) {
              return res.status(400).json({
                  success: false,
                  message: 'Fatal Server Error: ' + err
              });
          } else {
              req.decoded = decoded;
              next();
          }
      });
  } else {
      return res.status(201).json({
          success: false,
          message: 'Token is not available.'
      });
  }
};

const signup = (req, res, next) => {
    const email = req.body.email,
        password = req.body.password;

    console.log('Email: ' + email);
    console.log('Password: ' + password);

    if (!email || !password) {
        return res.status(201).json({
            success: false,
            message: 'Invalid or incomplete information.'
        });
    } else {
        Admin.findOne({email: email} , (err, admin) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    message: 'Fatal Server Error: ' + err
                });
            } else if (admin) {
                return res.status(201).json({
                    success: false,
                    message: 'Email already exists.'
                });
            } else {
                const admin = new Admin({
                    email: email,
                    password: password
                });
                admin.save((err, newAdmin) => {
                    if (err) {
                        return res.status(400).json({
                            success: false,
                            message: 'Fatal Server Error: ' + err
                        });
                    } else {
                        return res.status(201).json({
                            success: true,
                            message: 'Successfully created admin.',
                            data: newAdmin
                        });
                    }
                });
            }
        });
    }
};

const login = (req, res, next) => {
    const email = req.body.email,
        password = req.body.password;

    console.log('Email: ' + email);
    console.log('Password: ' + password);

    if (!email || !password) {
        return res.status(201).json({
            success: false,
            message: 'Invalid or incomplete information.'
        });
    } else {
        Admin.findOne({ email: email }, (err, admin) => {

            console.log('Admin from database');
            console.log(admin)

            if (err) {
                console.log('Error : ' + err);
                return res.status(401).json({
                    success: false,
                    message: 'Fatal Server Error: ' + err
                });
            } else if (!admin) {
                console.log('Not a valid email pass');
                return res.status(201).json({
                    success: false,
                    message: 'Invalid email.'
                });
            } else {
                console.log('Found email');
                admin.comparePassword(password, (err, isMatch) => {
                    if (isMatch && !err) {
                        const token = jwt.sign(admin, config.secret, {
                            expiresIn: config.tokenexp
                        });

                        return res.status(201).json({
                            success: true,
                            message: 'Successfully logged In.',
                            token: token
                        });
                    } else {
                        return res.status(400).json({
                            success: false,
                            message: 'Invalid credential'
                        });
                    }
                });
            }
        });
    }
};

module.exports = {
    authenticate,
    signup,
    login
};
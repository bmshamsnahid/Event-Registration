const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const participantSchema = new Schema({
    name: { type: String, required: true },
    rollNo: { type: String, required: true },
    fatherName: { type: String, required: true },
    dateOfBirth: { type: String, required: true },
    address: { type: String, required: true },
    email: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    educationalQualification: { type: String, required: true },
    isApproved: { type: Boolean, required: true, default: false },
    trackingNumber: { type: String, required: false }
});

module.exports = mongoose.model('Participant', participantSchema, 'participant');

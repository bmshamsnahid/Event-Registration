const mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    Schema = mongoose.Schema;

const AdminSchema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true }
});

AdminSchema.pre('save', function (next) {
    const Admin = this,
        SALT_FACTOR = 5;

    if (!Admin.isModified('password')) return next();

    bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
        if (err) return next(err);

        bcrypt.hash(Admin.password, salt, null, (err, hash) => {
            if (err) return next(err);
            Admin.password = hash;
            next();
        });
    });
});

AdminSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) { return cb(err); }

        cb(null, isMatch);
    });
};

module.exports = mongoose.model('Admin', AdminSchema, 'Admin');
